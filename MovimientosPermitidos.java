//clase que determinara los movimientos y jugadas permitidas , necesita hablar con tablero, necesita los resultados de los metodos 

public class MovimientosPermitidos{
	
	private Tablero tablero;

	public MovimientosPermitidos(Tablero tablero){
		this.tablero = tablero;
	}// fin del metodo constructor
	
	//Este  metodo funciona para eliminar una piedra que se encuentre en una esquina
	public void espacioRodeadoEsquina(int x, int y){
		if(x==0 & y==1 || x==1 & y==0 ){ 
			if(tablero.getPiedra(0,1)!=null & tablero.getPiedra(1,0)!=null){
				if(tablero.getColorPiedra(0,1).equals("Blanca") & tablero.getColorPiedra(1,0).equals("Blanca")){	
					if(tablero.getColorPiedra(0,0)!="Blanca" ){
						tablero.eliminarPiedra(0,0);//esquina superior izquierda
					}
				}else{
					if(tablero.getColorPiedra(0,1).equals("Negra") & tablero.getColorPiedra(1,0).equals("Negra")){
						if(tablero.getColorPiedra(0,0)!="Negra" ){
							tablero.eliminarPiedra(0,0);//esquina superior izquierda
						}
					}
				}
			} 
		} //fin if superior izquierda
		if(x==0 & y==7 || x==1 & y==8 ){ 
			if(tablero.getPiedra(0,7)!=null & tablero.getPiedra(1,8)!=null){
				if(tablero.getColorPiedra(0,7).equals("Blanca") & tablero.getColorPiedra(1,8).equals("Blanca")){	
					if(tablero.getColorPiedra(0,8)!="Blanca" ){
						tablero.eliminarPiedra(0,8);//esquina superior derecha
					}
				}else{
					if(tablero.getColorPiedra(0,7).equals("Negra") & tablero.getColorPiedra(1,8).equals("Negra")){
						if(tablero.getColorPiedra(0,8)=="Blanca" ){
							tablero.eliminarPiedra(0,8);//esquina superior derecha
						}
					}
				} 
			}
		}//fin if superior derecha
		if(x==8 & y==1 || x==7 & y==0 ){ 
			if(tablero.getPiedra(8,1)!=null & tablero.getPiedra(7,0)!=null){
				if(tablero.getColorPiedra(8,1).equals("Blanca") & tablero.getColorPiedra(7,0).equals("Blanca")){
					if(tablero.getColorPiedra(8,0)!="Blanca" ){
						tablero.eliminarPiedra(8,0);//esquina inferior izquierda
					} 
				}else{
					if(tablero.getColorPiedra(8,1).equals("Negra") & tablero.getColorPiedra(7,0).equals("Negra")){
						if(tablero.getColorPiedra(8,0)!="Negra" ){
							tablero.eliminarPiedra(8,0);//esquina inferior izquierda
						} 
					}
				}	
			}
		}//fin if inferior izquierda
		if(x==7 & y==8 || x==8 & y==7 ){ 
			if(tablero.getPiedra(7,8)!=null & tablero.getPiedra(8,7)!=null){
				if (tablero.getColorPiedra(7,8).equals("Blanca") & tablero.getColorPiedra(8,7).equals("Blanca")){
					if(tablero.getColorPiedra(8,8)!="Blanca" ){
						tablero.eliminarPiedra(8,8);//esquina inferior derecha
					} 
				}else{
					if(tablero.getColorPiedra(7,8).equals("Negra") & tablero.getColorPiedra(8,7).equals("Negra")){
						if(tablero.getColorPiedra(8,8)!="Negra" ){
							tablero.eliminarPiedra(8,8);//esquina inferior derecha
						} 
					}
				}
			}
		}//fin ifinferior derecha
		}	//fin metodo espacioRodeadoEsquina

	//Este  metodo funciona para eliminar una piedra que se encuentre en un borde(incompleto/por verificar)
	public void espacioRodeadoBordeIzquierdo(int x, int y){
		if(tablero.getPiedra(x+1,y+1)!=null & tablero.getPiedra(x+2,y)!=null) {
			tablero.eliminarPiedra(x+1,y);
		}//Si se coloca la piedra arriba de la que se desea eliminar
		if (tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x-1,y)!=null){
			tablero.eliminarPiedra(x-1,y);
		}//Si se coloca la piedra abajo de la que se desea eliminar
		if (tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x+1,y-1)!=null){
			tablero.eliminarPiedra(x,y-1);
		}//Si se coloca la piedra a la derecha de la que se desea eliminar
	}//fin del metodo espacioRodeadoBordeIzquierda
	
	public void espacioRodeadoBordeArriba(int x, int y){
		if(x==0){
				if(tablero.getPiedra(x,y+2)!=null & tablero.getPiedra(x+1,y+1)!=null) {
					tablero.eliminarPiedra(x,y+1);
					//Si se coloca la piedra izquierda de la que se desea eliminar
				}else{
					if(tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x,y-2)!=null) {
						tablero.eliminarPiedra(x,y-1);
						//Si se coloca la piedra a la derecha de la que se desea eliminar
					}
				}
		}else{
			if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x-1,y+1)!=null) {
				tablero.eliminarPiedra(x-1,y);
			}//Si se coloca la piedra abajo de la que se desea eliminar
		}
	}//fin del metodo espacioRodeadoBordeArriba

	public void espacioRodeadoBordeDerecho(int x, int y){
		if(y==8){
			if(tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x+2,y)!=null) {
				tablero.eliminarPiedra(x+1,y);
				//Si se coloca la piedra arriba de la que se desea eliminar
			}else{
				if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x-2,y)!=null) {
					tablero.eliminarPiedra(x-1,y);
				}//Si se coloca la piedra abajo de la que se desea eliminar
			}
		}else{
			if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x+1,y+1)!=null) {
				tablero.eliminarPiedra(x,y+1);
			}//Si se coloca la piedra izquierda de la que se desea eliminar
		}
	}//fin del metodo espacioRodeadoBordeDerecho
	
	public void espacioRodeadoBordeAbajo(int x, int y){
		if(x==8){
			if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x,y+2)!=null) {
				tablero.eliminarPiedra(x,y+1);
				//Si se coloca la piedra izquierda de la que se desea eliminar
			}else{
				if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x,y-2)!=null) {
				tablero.eliminarPiedra(x,y-1);
				//Si se coloca la piedra a la derecha de la que se desea eliminar
				}
			}
		}else{
			if(tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x+1,y+1)!=null) {
				tablero.eliminarPiedra(x+1,y);
			}//Si se coloca la piedra arriba de la que se desea eliminar
		}
	}//fin del metodo espacioRodeadoBorde
	
	//Este  metodo funciona para eliminar una piedra que se encuentre en la parte central del tablero
	public void espacioRodeadoCentral(int x, int y){
		//si la piedra que se coloco esta abajo de la piedra rodeada 3,3
		if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x-2,y)!=null & tablero.getPiedra(x-1,y-1)!=null){
			tablero.eliminarPiedra(x-1,y);
		}	
		//si la piedra que se coloco esta arriba de la piedra rodeada 
		if(tablero.getPiedra(x+1,y+1)!=null & tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x+2,y)!=null){
			tablero.eliminarPiedra(x+1,y);
		}
		//si la piedra que se coloco esta al lado derecho de la piedra rodeada
		if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x,y-2)!=null & tablero.getPiedra(x+1,y-1)!=null){
			tablero.eliminarPiedra(x,y-1);
		}
		//si la piedra que se coloco esta al lado izquierdo de la piedra rodeada
		if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x,y+2)!=null & tablero.getPiedra(x+1,y+1)!=null){
			tablero.eliminarPiedra(x,y+1);
		}
	}//fin del metodo espacioRodeadoCentral
	
	

	public boolean situacionKo (boolean espacioDisponible) {//no es necesario por ahora, falta desarrollarlo
		return true;
	}//fin del metodo SituacionKo

		//preguntar si esta vacio o no, CAMBIAR NOMBRE Y DIVIDIR LAS TAREAS, Y VERIFICAR SI SON O NO DEL TABLERO
	/* public boolean comprobarEspacioDisponible (int x, int y, String color){//metodo para definir si una piedra se puede o no colocar, segun si se quiere colocar en una esquina, borde u otro lado
		boolean colocarPiedra = false;
		if (esEsquina(x,y)){
			if(x==0){//primera fila, 
				if(y==0){//primera columna
					if(interseccion[x+1][0]==null || interseccion[0][y+1]==null || interseccion[x+1][0].getColor().equals(color) || interseccion[0][y+1].getColor().equals(color)){
						colocarPiedra = true;
						} else {
							colocarPiedra = false;
							}
					}//primera columna
					 else {
						if(interseccion[x+1][8] == null || interseccion[x][y-1]==null || interseccion[x+1][8].getColor().equals(color) || interseccion[x][y-1].getColor().equals(color)){//ultima columna
							colocarPiedra = true;
							}//ultima columna
							else {
								colocarPiedra = false;
								}
						}
				} else {
					if (y==0){
						if(interseccion[x-1][y]==null || interseccion[x][y+1]==null || interseccion[x-1][y].getColor().equals(color) || interseccion[x][y+1].getColor().equals(color)){
							colocarPiedra = true;
							} else {
								colocarPiedra = false;
								} 
						} else {
							if(interseccion[x-1][8]==null || interseccion[x][y-1]==null || interseccion[x-1][8].getColor().equals(color) || interseccion[x][y-1].getColor().equals(color)){
								colocarPiedra = true;
								} else {
									colocarPiedra = false;
									}
							}
					}//primera fila
			} else{
				if(esBorde(x,y)){
					if(x==0){
						if(interseccion[x][y+1]==null || interseccion[x][y-1]==null || interseccion[x+1][y]==null || interseccion[x][y+1].getColor().equals(color) || interseccion[x][y-1].getColor().equals(color) || interseccion[x+1][y].getColor().equals(color)){
							colocarPiedra = true;
							} else{
								colocarPiedra = false;
								} 
						} else{
							if(x==8){
								if (interseccion[x][y+1]==null || interseccion[x][y-1]==null || interseccion[x-1][y]==null || interseccion[x][y+1].getColor().equals(color) || interseccion[x][y-1].getColor().equals(color) || interseccion[x-1][y].getColor().equals(color)){
									colocarPiedra = true;
									} else {
										colocarPiedra = false;
										}
								}
							} 
					if(y==0){
						if(interseccion[x+1][y]==null || interseccion[x-1][y]==null || interseccion[x][y+1]==null || interseccion[x+1][y].getColor().equals(color) || interseccion[x-1][y].getColor().equals(color) || interseccion[x][y+1].getColor().equals(color)){
							colocarPiedra = true;
							} else{
								colocarPiedra = false;
								}
						} else{
							if(y==8){
								if(interseccion[x+1][y]==null || interseccion[x-1][y]==null || interseccion[x][y-1]==null || interseccion[x+1][y].getColor().equals(color) || interseccion[x-1][y].getColor().equals(color) || interseccion[x][y-1].getColor().equals(color)){
									colocarPiedra = true;
									} else{
										colocarPiedra = false;
										}
								}
							}	
					} else{
						if(interseccion[x+1][y]==null || interseccion[x-1][y]==null || interseccion[x][y-1]==null || interseccion[x][y+1]==null || interseccion[x+1][y].getColor().equals(color) || interseccion[x-1][y].getColor().equals(color) || interseccion[x][y-1].getColor().equals(color) || interseccion[x][y+1].getColor().equals(color)){
							colocarPiedra = true;
							} else {
								colocarPiedra = false;
								}
						}
				}
			return colocarPiedra;
			
		}*/

}// fin clase 
