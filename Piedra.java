//clase Piedra para el proyecto programado del Juego Go - Fundamentos de programacion ITM

public class Piedra{

	private int x, y;
	private String color, imagen;
	
	public Piedra(String color, String imagen){
		setColor(color);
		setImagen(imagen);
		//setX(x);
		//setY(y);
	    }//fin metodo constructor

	public void setColor(String color){
		this.color = color;
		}//fin metodo 

	public String getColor(){
		return color;
		}

	public void setX(int x){
		this.x = x;
		}
		
	public int getX(){
		return x;
		}
		
	public void setY(int y){
		this.y = y;
	    }
		
	public int getY(){
		return y;
		}
		
	public void setImagen(String imagen){
		this.imagen = imagen;
		}
		
	public String getImagen(){
		return imagen;
		}
	public String toString(){
		return getColor();
	}
}//fin clase Piedra
