import javax.swing.JOptionPane;

public class JuegoGo{
	public static void main (String arg[]){
		String nombreJugadorUno="", nombreJugadorDos="";
		
		Tablero tablero= new Tablero();
		Partida primeraPartida= new Partida(true, nombreJugadorUno,nombreJugadorDos);
		MovimientosPermitidos movimientosPermitidos = new MovimientosPermitidos(tablero);
		
		System.out.println(tablero.toString());
		
		//prueba metodo comprobarVacio
		if(tablero.comprobarVacio(2,3)){
			tablero.setPiedra(new Piedra("Blanca"," "),2,3);
		} 
		System.out.println(tablero.toString());

		if(tablero.comprobarVacio(2,3)){
			tablero.setPiedra(new Piedra("Negra"," "),2,3);
		} 
		System.out.println(tablero.toString());
		
		if(tablero.comprobarVacio(3,3)){
			tablero.setPiedra(new Piedra("Negra"," "),3,3);//abajo
		} 
		System.out.println(tablero.toString());
		
		//prueba metodo EspacioRodeadoEsquina
		tablero.setPiedra(new Piedra("Blanca"," "),8,8);			
		tablero.setPiedra(new Piedra("Negra"," "),7,8);
		tablero.setPiedra(new Piedra("Negra"," "),8,7);
		movimientosPermitidos.espacioRodeadoEsquina(8,7);
		System.out.println(tablero.toString());	
		
		//prueba metodo EspacioRodeadoCentro
		/*tablero.setPiedra(new Piedra("Negra"," "),2,4);//derecha
		tablero.setPiedra(new Piedra("Negra"," "),1,3);//arriba
		tablero.setPiedra(new Piedra("Negra"," "),2,2);//izquierda
		System.out.println(tablero.toString());
		movimientosPermitidos.espacioRodeadoCentral(3,3);
		System.out.println(tablero.toString());
		
		
		//prueba metodo EspacioRodeadoBorde
		//borde izquierdo
		tablero.setPiedra(new Piedra("Blanca"," "),1,0);
		tablero.setPiedra(new Piedra("Negra"," "),2,0);
		tablero.setPiedra(new Piedra("Blanca"," "),3,0);
		tablero.setPiedra(new Piedra("Blanca"," "),2,1);
		movimientosPermitidos.espacioRodeadoBordeIzquierdo(2,1);
		
		//borde arriba
		tablero.setPiedra(new Piedra("Blanca",""),0,1);
		tablero.setPiedra(new Piedra("Negra"," "),0,2);
		tablero.setPiedra(new Piedra("Blanca"," "),0,3);
		tablero.setPiedra(new Piedra("Blanca"," "),1,2);
		movimientosPermitidos.espacioRodeadoBordeArriba(1,2);
	
		
		//borde derecha
		tablero.setPiedra(new Piedra("Blanca"," "),1,8);
		tablero.setPiedra(new Piedra("Negra"," "),2,8);
		tablero.setPiedra(new Piedra("Blanca"," "),3,8);
		tablero.setPiedra(new Piedra("Blanca"," "),2,7);
		movimientosPermitidos.espacioRodeadoBordeDerecho(1,8);
		
	
		//borde abajo
		tablero.setPiedra(new Piedra("Blanca"," "),8,4);
		tablero.setPiedra(new Piedra("Negra"," "),8,5);
		tablero.setPiedra(new Piedra("Blanca"," "),8,6);
		tablero.setPiedra(new Piedra("Blanca"," "),7,5);
		movimientosPermitidos.espacioRodeadoBordeAbajo(8,4);
		
		System.out.println(tablero.toString());
		
		
		/* nombreJugadorUno=JOptionPane.showInputDialog("Digite el nombre del jugador uno");
		primeraPartida.setNombreJugadorUno(nombreJugadorUno);
		primeraPartida.setTurno(true);
		nombreJugadorDos=JOptionPane.showInputDialog("Digite el nombre del jugador dos");
		primeraPartida.setNombreJugadorDos(nombreJugadorDos);
		
		
		while(1<10){
			if (primeraPartida.getTurno()){
				JOptionPane.showMessageDialog(null,"Es el turno del jugador "+nombreJugadorUno);
				int x= Integer.parseInt(JOptionPane.showInputDialog("Digite la fila en la que desea poner la ficha"));
				int y= Integer.parseInt(JOptionPane.showInputDialog("Digite la columna en la que desea poner la ficha")); 
				if(tablero.comprobarVacio(x,y) && tablero.comprobarEspacioDisponible(x,y,"Negra")){
					tablero.setPiedra(new Piedra("Negra", x, y, "imagen"), x, y);
					primeraPartida.setTurno(true);
				}else { 
					JOptionPane.showMessageDialog(null,"movimiento invalido");
				}//fin del if/else
			System.out.println(tablero.toString());
			} //fin del if
			
			if(!primeraPartida.getTurno()){
				JOptionPane.showMessageDialog(null,"Es el turno del jugador "+nombreJugadorDos);
				int x= Integer.parseInt(JOptionPane.showInputDialog("Digite la fila en la que desea poner la ficha"));
				int y= Integer.parseInt(JOptionPane.showInputDialog("Digite la columna en la que desea poner la ficha")); 
				if(tablero.comprobarVacio(x,y) && tablero.comprobarEspacioDisponible(x,y,"Blanca")){
					tablero.setPiedra(new Piedra("Blanca", x, y, "imagen"), x, y);
					primeraPartida.setTurno(false);
				}else { 
					JOptionPane.showMessageDialog(null,"movimiento invalido");
				}//fin del if/else
				System.out.println(tablero.toString());
			}//fin del if
		}//fin del while*/
	}//Fin del main
}//Fin de la clase
