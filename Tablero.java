// esta clase es la clase Tablero, dentro de sus funciones esta crear el tablero una vez iniciada
// la partida, y comprobar el encierro de las piedras, los espacios disponibles para jugar.

public class Tablero{//declaracion de la clase
	
	//Atributos
	private Piedra interseccion[][];//declaracion de arreglo bidimensional que representara las intersecciones del tablero
	
	//Método constructor, no recibe parametros dado que de recibir recibiria el tamaño del tablero, pero este va a ser siempre el mismo
	public Tablero (){
		interseccion = new Piedra [9][9];//el tablero se creara con medida de 9x9
	}//fin del metodo tablero
	
	public void setPiedra(Piedra piedra, int x, int y){//¿x y y?
		interseccion[x][y]=piedra;
	}
	
	public Piedra getPiedra(int x, int y){
		return interseccion[x][y];
	}
	
	public String getColorPiedra(int x, int y){
		return interseccion[x][y].getColor();
	}
	
	//Comprobar si una piedra esta encerrada o no
	public boolean comprobarEncierro (int x, int y){
		if(getPiedra(x+1,y)!=null & getPiedra(x,y+1)!=null & getPiedra(x-1,y)!=null & getPiedra(x,y-1)!=null){
			return true;
		}else{
			return false;
		}
	}//fin del metodo comprobarEncierro
	
	public void eliminarPiedra(int x, int y){
		interseccion[x][y]=null;
	}
	
	//Métodos length
	public int lengthX(){
		return interseccion.length;
	}
	
	public int lengthY (int x){
		return interseccion[x].length;
	}
	
	public boolean esEsquina(int x, int y){//metodo para saber si la posicion es una esquina
		if(x==8 && y==8 || x==0 && y==0 || x==0 && y==8 || x==8 && y==0) {
			return true;
		} else {
			return false;
			}
	}//fin metodo esEsquina
	
	public boolean esBorde(int x, int y){//llamar cuando la esquina retorna false, para evaluar si la posicion es un borde 
		if (y==0 & x==1 || x==2 || x==3 || x==4 || x==5 || x==6 || x==7){
			return true;
		} else {
			if (y==8 & x==1 || x==2 || x==3 || x==4 || x==5 || x==6 || x==7){
				return true;
			} else {
				if (x==0 & y==1 || y==2 || y==3 || y==4 || y==5 || y==6 || y==7){
					return true;
				} else {
					if (x==8 & y==1 || y==2 || y==3 || y==4 || y==5 || y==6 || y==7){
						return true;
					} else {
						return false;
					}
				}
			}
		}//fin del if/else
	}//fin metodo esBorde	
	
	public boolean comprobarVacio(int x, int y){
		if(interseccion[x][y]!=null){
			return false;
		} else {
			return true;//si es true se puede colocar 
		}
		}//fin del metodo comprobarVacio	
	
	public String toString(){
		String  listaTablero=" ";
		for(int x=0; x<interseccion.length; x++){
			for(int y=0; y<interseccion[x].length; y++){
				listaTablero+="["+interseccion[x][y]+"]  ";
			}
			listaTablero+="\n";
		}//fin del for
		return listaTablero;
	}//fin del toString
	
}//Fin de la clase
