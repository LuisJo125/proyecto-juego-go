public class Partida{
	//Atributos y variables
	private boolean turno;
	private String nombreJugadorUno, nombreJugadorDos, ganador;
	private int prisionerosJugadorUno, prisionerosJugadorDos;
	
	//Método constructor
	public Partida (boolean turno, String nombreJugadorUno, String nombreJugadorDos){
		setTurno(turno);
		setNombreJugadorUno(nombreJugadorUno);
		setNombreJugadorDos(nombreJugadorDos);
	}
	
	//Métodos set, get y otros
	public void setTurno (boolean turno){
		if (turno){
			this.turno = false;//false para jugador dos 
			//return "Es el turno del jugador dos"; es void, no debe retornar nada 
		}//Fin if
		else{
			this.turno = true;//true para jugador uno
			//return "Es el turno del jugador uno"; es void, no debe retornar nada 
		}//Fin else
	}//Fin setTurno
	
	
	public boolean getTurno (){
		return turno;
	}//Fin getTurno
	
	
	public void pasarTurno (boolean turno){
		if (turno){
			this.turno = false;
		}//Fin if
		else{
			this.turno = true;
		}//Fin else
	}//Fin pasarTurno
	
	
	public void setNombreJugadorUno (String nombreJugadorUno){
		this.nombreJugadorUno=nombreJugadorUno;
	}//Fin setNombreJugadorUno
	
	
	public String getNombreJugadorUno (){
		return nombreJugadorUno;
	}//Fin getNombreJugadorUno
	
	
	public void setNombreJugadorDos (String nombreJugadorDos){
		this.nombreJugadorDos=nombreJugadorDos;
	}//Fin setNombreJugadorDos
	
	
	public String getNombreJugadorDos (){
		return nombreJugadorDos;
	}//Fin getNombreJugadorDos
	
	
	public void setPrisionerosJugadorUno(int prisionerosJugadorUno){		
		this.prisionerosJugadorUno= prisionerosJugadorUno;	
	}//fin de setPrisioneroJugadorUno
			
		
	public int getPrisionerosJugadorUno(){		
		return prisionerosJugadorUno;
	}//fin del getPrisioneroJugadorUno	
		
	
	public void setPrisionerosJugadorDos(int prisionerosJugadorDos){		
		this.prisionerosJugadorDos= prisionerosJugadorDos;	
	}//fin de setPrisioneroJugadorDos
	
	
	public int getPrisionerosJugadorDos(){		
		return prisionerosJugadorDos;
	}//Fin getPrisionerosJugadorDos
	
	
	public void setGanador (String nombreJugador){
		this.ganador = nombreJugador;
	}//Fin setGanador
	
	
	public String getGanador (){
		return ganador;
	}//Fin getGanador
	
	public String toString (){
		return "Nombre jugador 1: "+getNombreJugadorUno()+"\nPrisioneros jugador 1: "+getPrisionerosJugadorUno()+"\nNombre jugador 2: "+getNombreJugadorDos()+"\nPrisioneros jugador 2: "+getPrisionerosJugadorDos();
	}//Fin toString
	
	
	
}//Fin de la clase
